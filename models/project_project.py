# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class project_project(models.Model):
    _inherit = 'project.project'

    url_endpoint = fields.Char(
    )
    odoo_version = fields.Char(
    )
    notes = fields.Text(
    )
    master_repo = fields.Char(
    )
    ip_url = fields.Char(
    )
    user = fields.Char(
    )
    password = fields.Char(
    )
    managed = fields.Boolean(
    )
    endpoint = fields.Char(
    )
