# -*- coding: utf-8 -*-
{
    'name': 'project_developer_info',
    'version': '13.0.1.0.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'project',
    ],
    'data': [
        # security
        # data
        # templates
        'templates/task.xml',
        # reports
        # views
        'views/project_project.xml',
        'views/project_task.xml',
    ],
}
